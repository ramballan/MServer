import objects.Command;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import static objects.CommandType.*;

/**
 * This thread class handling commands from BlockingQueue and sleep when Queue is empty.
 */
public class CommandHandler extends Thread{
    private static BlockingQueue<Command> commandsQueue = new ArrayBlockingQueue<Command>(10,true);
    WindowManager wm = new WindowManager();
    @Override
    public void run() {
        while (true){
            Command command;
            try {
                command = commandsQueue.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
                continue;
            }
            switch (command.getCommandType()){
                case ShowPicture: wm.showPicture(command.getPictureURL(),command.getTimeout());
                    break;
                case ShowText: wm.showText(command.getMessageText(),command.getTimeout());
                    break;
            }
        }
    }

    /**
     * Add command to command queue
     * @param command command for execute
     */
    public static void addCommand(Command command) throws InterruptedException {
        commandsQueue.put(command);
    }
}
