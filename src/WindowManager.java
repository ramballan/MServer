import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class WindowManager {
    private JFrame fr = new JFrame("Youtube plugin for Opera");
    private final float pixPerSymbol = 25.2105f;
    public WindowManager() {
        fr.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        fr.setUndecorated(true);
        fr.setAlwaysOnTop(true);
        fr.addWindowStateListener(new WindowStateListener() {
            @Override
            public void windowStateChanged(WindowEvent e) {
                if ((e.getNewState() & WindowEvent.WINDOW_ICONIFIED) != 0) fr.setExtendedState(Frame.NORMAL);
            }
        });
        ImageIcon frameIcon = new ImageIcon(getClass().getResource("/images/youtube.png"));
        fr.setIconImage(frameIcon.getImage());
    }

    public void showPicture(URL pictureUrl, int timeout){
        ImageIcon icon = new ImageIcon(pictureUrl);
        JLabel label = new JLabel(icon);
        fr.getContentPane().add(label);
        fr.pack();
        fr.setVisible(true);
        fr.setLocationRelativeTo(null);
        try {
            TimeUnit.SECONDS.sleep(timeout);
            fr.getContentPane().removeAll();
            fr.dispose();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void showText(String message, int timeout){
        Dimension sSize = Toolkit.getDefaultToolkit ().getScreenSize ();
        JLabel label;
        //split long message if necessary
        if (sSize.getWidth()>pixPerSymbol*message.length()){
            label = new JLabel(message);
        }else {
            StringBuilder sbResult = new StringBuilder();
            sbResult.append("<html>");
            StringBuilder sbBuff = new StringBuilder();
            int maxSymbPerLine = (int) (sSize.width/pixPerSymbol);

            for (String s : message.split(" ")) {
                if (sbBuff.length()<maxSymbPerLine) sbBuff.append(s+" ");
                else{
                    sbBuff.append("<br>"+s+" ");
                    sbResult.append(sbBuff.toString());
                    sbBuff = new StringBuilder();
                }
            }
            sbResult.append(sbBuff.toString());
            sbResult.append("</html>");
            label = new JLabel(sbResult.toString());
        }

        Font font = new Font("Verdana", Font.PLAIN, 40);
        label.setFont(font);
        fr.setSize(sSize);
        label.setAutoscrolls(true);
        fr.getContentPane().add(label);
        fr.pack();
        fr.setVisible(true);
        fr.setLocationRelativeTo(null);
        try {
            TimeUnit.SECONDS.sleep(timeout);
            fr.getContentPane().removeAll();
            fr.dispose();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
