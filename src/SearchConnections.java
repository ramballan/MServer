import java.io.IOException;
import java.net.*;

/**
 * This class is the Thread for answering on datagram sockets. Clients using datagram sockets for finding server in local net.
 */
public class SearchConnections extends Thread{
    private static final int connectionTimeout = 2000;
    @Override
    public void run() {
        while (true){
            DatagramSocket ds = null;
            Socket client = null;
            try {
                ds = new DatagramSocket(9999);
                byte [] buffer = new byte[1024];
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                ds.receive(packet);
                System.out.println("Search request from :" + packet.getAddress().getHostAddress());
                Thread.sleep(100);
                client = new Socket();
                client.setSoTimeout(connectionTimeout);
                client.connect(new InetSocketAddress(packet.getAddress().getHostAddress(), 9998), connectionTimeout);
                client.close();
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (client != null) {
                        client.close();
                    }
                    if (ds != null) {
                        ds.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
