package objects;

import java.io.Serializable;
import java.net.URL;

public class Command implements Serializable{
    private CommandType commandType;
    private URL pictureURL;
    private String messageText;
    private int timeout;

    public Command(URL pictureURL, int timeout) {
        this.commandType = CommandType.ShowPicture;
        this.pictureURL = pictureURL;
        this.timeout = timeout;
    }

    public Command(String messageText, int timeout) {
        this.commandType = CommandType.ShowText;
        this.messageText = messageText;
        this.timeout = timeout;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public URL getPictureURL() {
        return pictureURL;
    }

    public String getMessageText() {
        return messageText;
    }

    public int getTimeout() {
        return timeout;
    }
}
