public class Main {

    public static void main(String[] args) {
        new SearchConnections().start();
        new CommandHandler().start();
        new CommandConnections().start();

    }
}
