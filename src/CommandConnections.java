import objects.Command;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * This thread-class is using for receiving commands from clients.
 */
public class CommandConnections extends Thread{
    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(9997);
            Socket connection;
            while (true){
                connection = serverSocket.accept();
                try {
                    ObjectInputStream input = new ObjectInputStream(connection.getInputStream());
                    CommandHandler.addCommand((Command) input.readObject());
                }catch (EOFException e){
                    System.out.println("request without data");
                }
                finally {
                    connection.close();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
